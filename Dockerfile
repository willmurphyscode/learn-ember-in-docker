FROM danlynn/ember-cli:latest

RUN useradd -ms /bin/bash dev

USER node

CMD [ "/bin/bash" ]
